$(function() {
	var sec1 = $('.section-1'),
			sec2_title = $('.section-2 .section-title').offset().top,
			sec2_second_row = $('.section-2 .second-row').offset().top,
			sec3 = $('.section-3').offset().top,
			wHeight = $(window).height()
			sec1_bottom = $('.section-1 .section-1-bottom').offset().top;

	$(document).ready(function() {
		setTimeout(function() {
			$('.section-1 .block-left').removeClass('hidden');
			$('.section-1 .block-right').removeClass('hidden');
		}, 300)
		setTimeout(function() {
			$('.section-1 .block-right .img-border').addClass('animated');
		}, 850);
	});

	// On scroll
	$(window).on('scroll', function() {
		var wScroll = $(this).scrollTop();
		if(wScroll > sec1_bottom - (wHeight / 1.3)) {
			$('.section-1 .box').each(function(i) {
				var box = $(this);
				setTimeout(function(i) {
					box.removeClass('hidden');
				}, 250*(i+1));
			})
		}
		if (wScroll > sec2_title - (wHeight / 1.4)) {
			$('.section-2 .section-title').removeClass("hidden");
		}
		if (wScroll > sec2_title - (wHeight / 2)) {
			$('.section-2 .first-row .block-left').removeClass("hidden");
			setTimeout(function() {
				$('.section-2 .first-row .block-left .img-border').addClass('animated');
				$('.section-2 .first-row .block-right').removeClass("hidden");
			}, 250);
		}
		if (wScroll > sec2_second_row - (wHeight / 1.5)) {
			$('.section-2 .second-row .block-right').removeClass("hidden");
			setTimeout(function() {
				$('.section-2 .second-row .block-right .img-border').addClass('animated');
				$('.section-2 .second-row .block-left').removeClass("hidden");
			}, 250);
		}
		if (wScroll > sec3 - (wHeight / 1.5)) {
			$('.section-3').removeClass("hidden");
		}
	})
})