$(function() {
	var sec1 = $('.section-1'),
			section2Title1 = $('.section-2 .title-1').offset().top,
			section2Title2 = $('.section-2 .title-2').offset().top,
			section2TableHead = $('.section-2 .table-head').offset().top,
			section3Title = $('.section-3 .title').offset().top,
			section4 = $('.section-4').offset().top,
			wWidth = $(window).width();
			wHeight = $(window).height();

	$(document).ready(function() {
		setTimeout(function() {
			$('.section-1 .block-left').removeClass('hidden');
			$('.section-1 .block-right').removeClass('hidden');
		}, 300);
		setTimeout(function() {
			$('.section-1 .img-border').addClass('animated');
		}, 850);
	});
	
	// On scroll
	$(window).on('scroll', function() {
		var wScroll = $(this).scrollTop();
		if (wScroll > section2Title1 - (wHeight / 1.4)) {
			$('.section-2 .title-1').removeClass("hidden");
		}
		if (wScroll > section2Title1 - (wHeight / 1.6)) {
			$('.section-2 .box-wrap .box').each(function(i) {
				var box = $(this);
				setTimeout(function(i) {
					box.removeClass("hidden");
				}, 300*i);
			});
		}
		if (wScroll > section2Title2 - (wHeight / 1.5)) {
			$('.section-2 .title-2').removeClass("hidden");
			setTimeout(function() {
				$('.section-2 .table-wrap').removeClass("hidden")
			}, 550)
		}
		if (wScroll > section2TableHead - 90) {
			$('.section-2 .table-head').addClass('fixed');
		} else {
			$('.section-2 .table-head').removeClass('fixed');
		}
		if (wScroll > section3Title - (wHeight / 1.5)) {
			$('.section-3 .title').removeClass("hidden");
			setTimeout(function() {
				$('.section-3 .content').removeClass("hidden")
			}, 800)
		}
		if (wScroll > section4 - (wHeight / 1.1)) {
			$('.section-4').removeClass("hidden");
		}
	})
})