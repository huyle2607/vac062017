$(function () {
    $('.sidemenu-item').on('click', function () {
        // Toggle class for navigation
        $('.sidemenu-item').removeClass('active');
        $(this).addClass('active');
        // Show/hide content
        var contentID = $(this).attr('data-target');
        $('.block.show').addClass('closed');
        setTimeout(function () {
            $('.block.show.closed').removeClass('show closed');
            $(contentID).addClass('animate-in');
        }, 250);
        setTimeout(function () {
            $(contentID).addClass('show');
            $(contentID).removeClass('animate-in');
        }, 700);
    });

    $(window).ready(function () {
        $('.section-1 .block-left').removeClass('hidden');
        $('.section-1 .block-right').removeClass('hidden');
        setTimeout(function () {
            $('.section-1 .block-left .img-border').addClass('animated')
        }, 600);
    });

    var wHeight = $(window).height();
    var wWidth = $(window).outerWidth();
    var sidemenu = $('.sidemenu');
    var sidemenuPos = sidemenu.offset().top;
    var section2 = $('.section-2').offset().top;
    var sec3Title = $('.section-3 .title').offset().top;
    $(window).scroll(function () {
        var wScroll = $(this).scrollTop();
        if (wScroll > sidemenuPos) {
            sidemenu.addClass('fixed');
        } else {
            sidemenu.removeClass('fixed');
        }
        if (wScroll > section2 - (wHeight / 2.5)) {
            $('.section-2').removeClass('hidden');
        }
        if (wWidth > 767) {
            if (wScroll > sec3Title - (wHeight / 1.455)) {
                sidemenu.addClass('disable-fixed');
            } else {
                sidemenu.removeClass('disable-fixed');
            }
        } else {
            if (wScroll > sec3Title - (wHeight / 1.2)) {
                sidemenu.addClass('disable-fixed');
                setTimeout(function () {
                    sidemenu.addClass('hidden');
                }, 500);
            } else {
                sidemenu.removeClass('hidden');
                setTimeout(function () {
                    sidemenu.removeClass('disable-fixed');
                }, 200);
            }
        }
    })
});