$(function() {
	// Trang chủ
	$('.section-1 .row').slick({
		prevArrow: "<span class='arrow arrow-prev'></span>",
		nextArrow: "<span class='arrow arrow-next'></span>",
		appendArrows: $('.slide-arrows')
	});

	$('.section-2 .bottom-block-wrap').slick({
		arrows: false,
		mobileFirst: true,
		centerMode: true,
		centerPadding: '15px',
		responsive: [
			{
				breakpoint: 374,
				settings: {
					centerPadding: '30px'
				}
			},
			{
				breakpoint: 413,
				settings: {
					centerPadding: '50px'
				}
			},
			{
				breakpoint: 767,
				settings: {
					centerPadding: '120px'
				}
			},
			{
				breakpoint: 991,
				settings: {
					centerPadding: '0px',
					slidesToShow: 3
				}
			}
		]
	});

	$('.section-4 .s4-slide').slick({
		prevArrow: "<span class='arrow arrow-prev'></span>",
		nextArrow: "<span class='arrow arrow-next'></span>",
		appendArrows: $('.section-4 .arrows-wrap'),
		mobileFirst: true,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					dots: true,
					appendDots: $('.section-4 .dots-wrap'),
					dotsClass: 'slide-dots'
				}
			}
		]
	});

	// On scroll
	var sec2 = $('.section-2').offset().top,
		sec3 = $('.section-3').offset().top,
		sec4 = $('.section-4').offset().top,
		sec5 = $('.section-5').offset().top,
		wHeight = $(window).height(),
		flag = 1;
	$(window).on('scroll', function() {
		var wScroll = $(this).scrollTop();
		if(wScroll > sec2 - (wHeight / 1.5)) {
			$('.section-2 .top-block').each(function(i) {
				var item = $(this);
				setTimeout(function(i) {
					item.addClass('in');
				}, 250 * (i+1));
			});
		}
		if (wScroll > sec2 ) {
			$('.section-2 .bottom-block-wrap').addClass('in');
		}
		if (wScroll > $('.s2-bottom-info').offset().top - (wHeight / 1.2)) {
			$('.s2-bottom-info .info-1').addClass('in');
			$('.s2-bottom-info .info-2').addClass('in');
		}
		if (wScroll > sec3 - (wHeight / 1.2)) {
			$('.s3-title').addClass('in');
			$('.s3-sub-title').addClass('in');
		}
		if (wScroll > sec3 - (wHeight / 2.2)) {
			$('.section-3 .block').each(function(i) {
				var item = $(this);
				setTimeout(function(i) {
					item.addClass('in');
				}, 200 * (i+1));
			});
		}
		if (wScroll > sec4 - (wHeight / 2)) {
			$('.block-left').addClass('in');
			$('.block-right').addClass('in');
		}
		if (wScroll > sec5 - (wHeight / 1.5)) {
			$('.block').addClass('in');
		}
	});
});