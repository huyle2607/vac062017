var gulp 				= require('gulp'),
		jade 				= require('gulp-jade'),
		sass 				= require('gulp-sass'),
		cssnano 		= require('gulp-cssnano'),
		uglify 			= require('gulp-uglify'),
		plumber 		= require('gulp-plumber'),
		watch 			= require('gulp-watch'),
		notify 			= require('gulp-notify'),
		concat 			= require('gulp-concat'),
		imageMin 		= require('gulp-imagemin'),
		del 				= require('del'),
		runSequence = require('run-sequence'),
		browserSync = require('browser-sync').create();
// Path configuration
var config = {
	'dist': {
		'public': 'public',
		'sources' : 'sources'
	},
	'styles': {
		'src' : 'sources/scss/**/*.+(scss|sass|css)',
		'public' : 'public/assets/css'
	} ,
	'js': {
		'src' : 'sources/js/**/*',
		'public' : 'public/assets/js'
	} ,
	'jade': {
		'src' : 'sources/**/*.jade',
		'public' : 'public'
	} ,
	'html': {
		'src' : 'sources/**/*.html',
		'public' : 'public'
	} ,
	'fonts': {
		'src' : 'sources/fonts/**/*',
		'public' : 'public/assets/fonts'
	},
	'img': {
		'src' : 'sources/img/**/*',
		'public' : 'public/assets/img'
	}
}

var onError = function(err) {
  notify.onError({
    title: "Uh oh!",
    subtitle: "Failure!",
    message: "Error: <%= error.message %>",
    sound: "none"
  })(err);
  this.emit('end');
}

gulp.task('jade', function() {
	gulp.src(config.jade.src)
	.pipe(plumber({
		errorHandler: onError
	}))
	.pipe(jade({
		pretty: true
	}))
	.pipe(gulp.dest(config.jade.public))
	.pipe(browserSync.reload({
		stream: true
	}))
});

gulp.task('html', function() {
	gulp.src(config.html.src)
	.pipe(plumber({
		errorHandler: onError
	}))
	.pipe(gulp.dest(config.html.public))
	.pipe(browserSync.reload({
		stream: true
	}))
});

gulp.task('scss', function() {
	gulp.src(config.styles.src)
	.pipe(plumber({
		errorHandler: onError
	}))
	.pipe(sass({ includePaths: [config.styles.public] }))
	.pipe(concat('style.css'))
	.pipe(gulp.dest(config.styles.public))
	.pipe(browserSync.reload({
		stream: true
	}))
});

gulp.task('javascript', function() {
	gulp.src(config.js.src)
	// .pipe(concat('main.js'))
	.pipe(uglify())
	.pipe(gulp.dest(config.js.public))
	.pipe(browserSync.reload({
		stream: true
	}))
});

gulp.task('img', function() {
	gulp.src(config.img.src)
	.pipe(imageMin())
	.pipe(gulp.dest(config.img.public))
	.pipe(browserSync.reload({
		stream: true
	}))
});

gulp.task('font', function() {
	gulp.src(config.fonts.src)
	.pipe(gulp.dest(config.fonts.public))
	.pipe(browserSync.reload({
		stream: true
	}))
});

gulp.task('watch', function() {
	gulp.watch(config.jade.src, ['jade']);
	gulp.watch(config.html.src, ['html']);
	gulp.watch(config.styles.src, ['scss']);
	gulp.watch(config.js.src, ['javascript']);
	gulp.watch(config.img.src, ['img']);
	gulp.watch(config.fonts.src, ['font']);
});

gulp.task('browserSync', function() {
	browserSync.init({
		server: {
			baseDir: 'public'
		},
		open: false
	});
});

gulp.task('clean', function() {
	return del.sync(config.dist.public);
});

gulp.task('default', function() {
	runSequence(['clean', 'jade', 'html', 'scss', 'javascript', 'img', 'font', 'browserSync'], 'watch');
});
